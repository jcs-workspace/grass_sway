[![Unity Engine](https://img.shields.io/badge/unity-2022.2.8f1-black.svg?style=flat&logo=unity&cacheSeconds=2592000)](https://unity3d.com/get-unity/download/archive)

# Grass_Sway

Following tutorial [GRASS SWAY in Unity - SHADER GRAPH](https://www.youtube.com/watch?v=L_Bzcw9tqTc&ab_channel=Brackeys).

![demo](etc/demo.gif)